-- |

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Types where

import Control.Monad.Reader
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.Map.Strict (Map)
import Data.Maybe (fromJust)
import Data.Set (Set)
import System.Environment
import System.Exit
import Text.Read

-- Application Monad

data Env = Env
  { envServer :: String
  , envKey :: Maybe Integer
  , envApiKey :: String
  } deriving (Eq,Show,Read)

type AppM = ReaderT Env IO

externalServer :: String
externalServer = "https://icfpc2020-api.testkontur.ru"

defaultEnv :: String -> Maybe Integer -> Env
defaultEnv server playerKey = Env
  { envServer = server
  , envKey = playerKey
  , envApiKey = "27124547b965442f937029dfbd8c3552"
  }

parseArgs :: IO Env
parseArgs = do
  args <- getArgs
  case args of
    [server] -> pure $ defaultEnv server Nothing
    [server, keyStr] -> pure $ defaultEnv server $ readMaybe keyStr
    _ -> putStrLn "Usage: stack run <server> [<playerKey>]" >> die "Wrong usage"

envPlayerKey :: Env -> Integer
envPlayerKey = fromJust . envKey

setPlayerKey :: Integer -> Env -> Env
setPlayerKey pk env = env { envKey = Just pk }

runAppM :: String -> Integer -> AppM a -> IO a
runAppM server playerKey = runAppEnv $ defaultEnv server $ Just playerKey

runAppEnv :: Env -> AppM a -> IO a
runAppEnv env act = runReaderT act env

-- Evaluation Monad

type Screen = Set (Int,Int)

data F1
  = Inc | Dec | Neg | Pwr2
  | Mod | Dem
  | CombI
  | LCar | LCdr | LIsNil
  deriving (Eq,Show,Read)

data F2
  = Add | Mul | Quot | Eq | Lt
  | CombT | CombF
  | LCons
  deriving (Eq,Show,Read)

data F3
  = CombS | CombC | CombB
  | IfZero
  deriving (Eq,Show,Read)

data V
  = N Integer
  | W ByteString
  | Pic Screen
  | VCons V V
  | VNil
  deriving (Eq,Show,Read)

data CB = Picture | Checker | Video
  deriving (Eq,Show,Read)

data Tree
  = Fun String
  | Call1 F1
  | Call2 F2
  | Call3 F3
  -- | Callback CB
  -- | Transmit
  | Val V
  | App Tree Tree
  deriving (Eq,Show,Read)

type Binds = Map String Tree

data EvalEnv = EvalEnv
  { binds :: Binds
  , onFail :: String -> IO V
  , logAction :: Tree -> IO ()
  , debugging :: String -> IO ()
  -- , draw :: CB -> V -> IO ()
  -- , send :: V -> IO V
  }

type EvalM a = ReaderT EvalEnv IO (Res a)

data Res a
  = Fail String
  | NoProgress Tree
  | Success a
  | Continue Tree
  -- | Output CB V (EvalM a)
  -- | WaitInput V (V -> EvalM a)
  deriving (Eq,Show,Read,Functor)

failing :: String -> EvalM a
failing = pure . Fail

success :: a -> EvalM a
success = pure . Success

continue :: Tree -> EvalM a
continue = pure . Continue

instance MonadFail (Either String) where
  fail = Left

eitherToRes :: Either String a -> Res a
eitherToRes (Left err) = Fail err
eitherToRes (Right a)  = Success a
