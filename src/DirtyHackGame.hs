-- |

module DirtyHackGame where

import Types
import Game.Req

import Game.Strategy
import Game.Strategy1

import Control.Monad.IO.Class
import Control.Monad.Reader

theStrategy = stayAndShoot

gameMain :: IO ()
gameMain = do
  env <- parseArgs
  putStrLn ("ServerUrl: " ++ envServer env ++ "; PlayerKey: " ++ show (envKey env))
  runner theStrategy env $ do
    mk <- asks envKey
    case mk of
      Just k -> strategyGame theStrategy
      Nothing -> do
        (attacker, defender) <- createReq
        playerKey <- liftIO $ do
          putStrLn "Enter A for attacker and D for defender: "
          mode <- getLine
          pure $ case mode of
            "A" -> attacker
            "D" -> defender
        local (setPlayerKey playerKey) $ strategyGame theStrategy
