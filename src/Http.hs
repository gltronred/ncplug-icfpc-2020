-- |

{-# LANGUAGE OverloadedStrings #-}
module Http where

import Types

import Control.Exception (catch, SomeException)
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.ByteString.Lazy.UTF8 as BLU
import Data.ByteString.Lazy.Char8 as B
import Network.HTTP.Simple

-- | Sends message to proxy
-- Message is a string with 0 and 1, e.g. 1101000
send' :: (MonadIO m, MonadReader Env m) => String -> ByteString -> m (Either String ByteString)
send' url msg = do
  -- putStrLn ("ServerUrl: " ++ args!!0 ++ "; PlayerKey: " ++ args!!1)
  server <- asks envServer
  apiKey <- asks envApiKey
  request' <- liftIO $ parseRequest ("POST " ++ server ++ url ++ "?apiKey=" ++ apiKey)
  let request = setRequestBodyLBS msg request'
  response <- liftIO $ httpLBS request
  let statuscode = getResponseStatusCode response
      body = getResponseBody response
  case statuscode of
    200 -> do
      -- putStrLn ("Server response: " ++ BLU.toString (getResponseBody response))
      pure $ Right body
    _ -> do
      -- putStrLn ("Unexpected server response:\nHTTP code: " ++ statuscode ++ "\nResponse body: " ++ BLU.toString (getResponseBody response))
      pure $ Left $ "Unexpected: code=" ++ show statuscode ++ "\n" ++ BLU.toString body
  -- handler :: SomeException -> IO ()
  -- handler ex = putStrLn $ "Unexpected server response:\n" ++ show ex

aliensSend :: (MonadIO m, MonadReader Env m) => ByteString -> m (Either String ByteString)
aliensSend = send' "/aliens/send"

send msg = do
  liftIO $ B.putStrLn $ "<<< " <> msg
  send' "/aliens/send" msg
