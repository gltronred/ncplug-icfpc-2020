-- |

{-# LANGUAGE TupleSections #-}

module Compiler where

import Types hiding (binds)
import Language

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Set (Set)

data State = State
  { expr :: Maybe Tree
  , binds :: Binds
  } deriving (Eq,Show,Read)

compile :: Maybe Tree -> [[Lang]] -> Either String State
compile mexpr = fmap (State mexpr . M.fromList) . mapM compileDefn

-- | Compiles definition into lambda expression
compileDefn :: [Lang] -> Either String (String, Tree)
compileDefn (Variable fun : Definition : defn) = (show fun,) <$> compileBody fun defn
compileDefn (Named fun : Definition : defn) = (fun,) <$> compileBody fun defn
compileDefn line = Left $ "Don't know how to compile " ++ show (take 3 line)

compileBody :: Show a => a -> [Lang] -> Either String Tree
compileBody name body = let
  (expr, rest) = compileLine body
  in case rest of
       [] -> Right expr
       _ -> Left $ "Leftover in " ++ show name ++ ": " ++ show rest

compileLine :: [Lang] -> (Tree, [Lang])
compileLine [] = error "Fail to compile"
compileLine (x : rest) = case x of
  Number n -> (Val $ N n, rest)
  Successor -> (Call1 Inc, rest)
  Predecessor -> (Call1 Dec, rest)
  Sum -> (Call2 Add, rest)
  Variable f -> (Fun $ show f, rest)
  Product -> (Call2 Mul, rest)
  Division -> (Call2 Quot, rest)
  Equality -> (Call2 Eq, rest)
  LessThan -> (Call2 Lt, rest)
  Modulate -> (Call1 Mod, rest)
  Demodulate -> (Call1 Dem, rest)
  -- Send -> (Transmit, rest)
  Negate -> (Call1 Neg, rest)
  Apply -> let
    (f, rest') = compileLine rest
    (x, rest'') = compileLine rest'
    in (App f x, rest'')
  S -> (Call3 CombS, rest)
  C -> (Call3 CombC, rest)
  B -> (Call3 CombB, rest)
  BTrue -> (Call2 CombT, rest)
  BFalse -> (Call2 CombF, rest)
  Power2 -> (Call1 Pwr2, rest)
  I -> (Call1 CombI, rest)
  Cons -> (Call2 LCons, rest)
  Car -> (Call1 LCar, rest)
  Cdr -> (Call1 LCdr, rest)
  Nil -> (Val VNil, rest)
  IsNil -> (Call1 LIsNil, rest)
  -- Draw -> (Callback Picture, rest)
  -- Checkerboard -> (Callback Checker, rest)
  -- MultiDraw -> (Callback Video, rest)
  If0 -> (Call3 IfZero, rest)
  -- Interact -> (Callback Interact, rest)
  -- StatelessDraw -> (Callback "statelessdraw", rest)
  -- StatefulDraw -> (Callback "statefuldraw", rest)
  Named f -> (Fun f, rest)
