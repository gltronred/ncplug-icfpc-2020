-- |

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TupleSections #-}

module Interpreter where

import Types
import Modulation

import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Set (Set)

eval :: Tree -> EvalM V
eval expr = case expr of
  App (Call1 f) t -> apply1 f t
  App (App (Call2 f) t1) t2 -> apply2 f t1 t2
  App (App (App (Call3 f) t1) t2) t3 -> apply3 f t1 t2 t3
  -- App Transmit t -> error "Can't Transmit!" -- pure $ WaitInput (N 0) (\input -> eval t) -- TODO: fix
  -- App (Callback f) t -> error "Can't callback!" -- pure $ Output f VNil (eval t) -- TODO: fix
  App t1 t2 -> do
    r1 <- eval t1
    case r1 of
      Fail f1 -> failing $ f1 ++ "\nIn:\n" ++ show expr
      Success v1
        | t1 /= Val v1 -> continue $ App (Val v1) t2
        | otherwise -> pure $ NoProgress t1
      Continue t1' -> continue $ App t1' t2
      NoProgress _t1' -> do
        r2 <- eval t2
        case r2 of
          Fail f2 -> failing $ f2 ++ "\nIn:\n" ++ show expr
          Success v2
            | t2 /= Val v2 -> continue $ App t1 (Val v2)
            | otherwise -> pure $ NoProgress $ App t1 t2
          Continue t2' -> continue $ App t1 t2'
          NoProgress _t2' -> pure $ NoProgress $ App t1 t2
  Val v -> success v
  Fun f -> do
    bs <- asks binds
    l <- asks logAction
    liftIO $ l (Fun f)
    pure $ case M.lookup f bs of
      Nothing -> Fail $ "Can't find " ++ f
      Just fv -> Continue fv
  other -> pure $ NoProgress other

evalN :: Tree -> EvalM V
evalN t = do
  env <- ask
  r <- eval t
  case r of
    Fail reason -> do
      liftIO $ onFail env reason
      pure r
    NoProgress _t' -> failing $ "Stuck at: " ++ show t
    Success a -> pure $ Success a
    Continue t'
      | t' /= t -> do
          liftIO $ logAction env t'
          evalN t'
      | otherwise -> failing "Cycle detected"
  -- Output cb v next -> do
  --   draw env cb v
  --   runReaderT next env >>= evalN env
  -- WaitInput r next -> do
  --   v <- send env r
  --   runReaderT (next v) env >>= evalN env

apply1 :: F1 -> Tree -> EvalM V
apply1 Inc (Val (N n)) = success $ N $ n+1
apply1 Dec (Val (N n)) = success $ N $ n-1
apply1 Neg (Val (N n)) = success $ N $ negate n
apply1 Pwr2 (Val (N n)) = success $ N $ 2^n
apply1 Mod (Val v) = pure $ eitherToRes $ modulate v
apply1 Dem (Val (W w)) = pure $ eitherToRes $ demodulate w
apply1 CombI t = continue t
apply1 LCar (Val (VCons t1 t2)) = success t1
apply1 LCdr (Val (VCons t1 t2)) = success t2
apply1 LIsNil (Val (VCons _ _)) = continue $ Call2 CombT
apply1 f t = do
  r <- eval t
  case r of
    Fail f -> failing f
    Success v -> continue $ App (Call1 f) (Val v)
    Continue t' -> continue $ App (Call1 f) t'
    NoProgress _t' -> pure $ NoProgress t
-- apply1 f t1 = failing $ show f ++ "\n" ++ show t1
-- apply1 f t = apply1 f $ eval binds t

apply2 :: F2 -> Tree -> Tree -> EvalM V
apply2 Add (Val (N n1)) (Val (N n2)) = success $ N $ n1+n2
apply2 Mul (Val (N n1)) (Val (N n2)) = success $ N $ n1-n2
apply2 Quot (Val (N n1)) (Val (N n2)) = success $ N $ n1 `quot` n2
apply2 Eq (Val (N n1)) (Val (N n2)) = continue $ Call2 $ if n1 == n2 then CombT else CombF
apply2 Lt (Val (N n1)) (Val (N n2)) = continue $ Call2 $ if n1 < n2 then CombT else CombF
apply2 CombT t1 t2 = continue t1
apply2 CombF t1 t2 = continue t2
apply2 LCons (Val v1) (Val v2) = success $ VCons v1 v2
apply2 f t1 t2 = do
  r1 <- evalN t1
  case r1 of
    Fail f1 -> failing f1
    Success v1 -> continue $ App (App (Call2 f) (Val v1)) t2
    Continue t1' -> continue $ App (App (Call2 f) t1') t2
    NoProgress _t1' -> do
      r2 <- evalN t2
      case r2 of
        Fail f2 -> failing f2
        Success v2 -> continue $ App (App (Call2 f) t1) (Val v2)
        Continue t2' -> continue $ App (App (Call2 f) t1) t2'
        NoProgress _t2' -> pure $ NoProgress $ App (App (Call2 f) t1) t2
-- apply2 f t1 t2 = failing $ show f ++ "\n" ++ show t1 ++ "\n" ++ show t2

apply3 :: F3 -> Tree -> Tree -> Tree -> EvalM V
apply3 CombS x y z = continue $ App (App x z) (App y z)
apply3 CombC x y z = continue $ App (App x z) y
apply3 CombB x y z = continue $ App x $ App y z
apply3 IfZero (Val (N n)) t1 t2 = continue $ if n == 0 then t1 else t2
apply3 IfZero t0 t1 t2 = do
  r0 <- evalN t0
  case r0 of
    Fail f0 -> failing f0
    Success v0 -> continue $ App (App (App (Call3 IfZero) (Val v0)) t1) t2
    Continue t0' -> continue $ App (App (App (Call3 IfZero) t0') t1) t2
    NoProgress _t0' -> failing $ "Stuck at IfZero " ++ show t0
-- apply3 f t1 t2 t3 = failing $ show f ++ "\n" ++ show t1 ++ "\n" ++ show t2 ++ "\n" ++ show t3
