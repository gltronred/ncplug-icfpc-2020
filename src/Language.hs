module Language where

import Text.Read (readMaybe)

data Lang
  = Number Integer -- #1 #2 #3
  | Definition     -- #4 aka =
  | Successor      -- #5 aka inc
  | Predecessor    -- #6 aka dec
  | Sum            -- #7 aka add
  | Variable Int   -- #8 aka x0, x1, etc. Do we need it at all?
  | Product        -- #9 aka mul
  | Division       -- #10 aka div
  | Equality       -- #11 aka eq
  | LessThan       -- #12 aka lt
  | Modulate       -- #13 aka mod
  | Demodulate     -- #14 aka dem
  | Send           -- #15 aka send
  | Negate         -- #16 aka neg
  | Apply          -- #17 aka ap
  | S              -- #18 aka s
  | C              -- #19 aka c
  | B              -- #20 aka b
  | BTrue          -- #21 aka t
  | BFalse         -- #22 aka f
  | Power2         -- #23 aka pwr2
  | I              -- #24 aka i
  | Cons           -- #25 aka cons
  | Car            -- #26 aka car
  | Cdr            -- #27 aka cdr
  | Nil            -- #28 aka nil
  | IsNil          -- #29 aka isnil
  -- #30 is a list construction aka ( ... )
  -- #31 is a vector aka vec which is equal to cons
  | Draw           -- #32 aka draw
  | Checkerboard   -- #33 aka checkerboard
  | MultiDraw      -- #34 aka multipledraw
  -- #35 modlate list (vector form of mod)
  -- #36 `send ( 0 )`
  | If0            -- #37 aka if0
  | Interact       -- #38 aka interact
  -- #39 interaction protocol
  | StatelessDraw  -- #40 aka statelessdraw
  | StatefulDraw   -- #41 stateful drawing protocol
  | Named String   -- like 'galaxy'

parseLang :: String -> Lang
parseLang "="        = Definition
parseLang "inc"      = Successor
parseLang "dec"      = Predecessor
parseLang "add"      = Sum
parseLang "mul"      = Product
parseLang "div"      = Division
parseLang "eq"       = Equality
parseLang "lt"       = LessThan
parseLang "mod"      = Modulate
parseLang "dem"      = Demodulate
parseLang "send"     = Send
parseLang "neg"      = Negate
parseLang "ap"       = Apply
parseLang "s"        = S
parseLang "c"        = C
parseLang "b"        = B
parseLang "t"        = BTrue
parseLang "f"        = BFalse
parseLang "pwr2"     = Power2
parseLang "i"        = I
parseLang "cons"     = Cons
parseLang "car"      = Car
parseLang "cdr"      = Cdr
parseLang "nil"      = Nil
parseLang "isnil"    = IsNil
parseLang "draw"     = Draw
parseLang "checkerboard" = Checkerboard
parseLang "multipledraw" = MultiDraw
parseLang "if0"      = If0
parseLang "interact" = Interact
parseLang "statelessdraw" = StatelessDraw
parseLang "statefuldraw" = StatefulDraw
parseLang (':' : id) = Variable $ read id
parseLang f          = case readMaybe f of
  Just n -> Number n
  Nothing -> Named f
-- parseLang _       = error "Unsupported lang id."

parse :: String -> [[Lang]]
parse = map (map parseLang . words) . lines

instance Show Lang where
  show (Number n)    = show n
  show Definition    = "="
  show Successor     = "inc"
  show Predecessor   = "dec"
  show Sum           = "add"
  show (Variable i)  = ":" ++ show i
  show Product       = "mul"
  show Division      = "div"
  show Equality      = "eq"
  show LessThan      = "lt"
  show Modulate      = "mod"
  show Demodulate    = "dem"
  show Send          = "send"
  show Negate        = "neg"
  show Apply         = "ap"
  show S             = "s"
  show C             = "c"
  show B             = "b"
  show BTrue         = "t"
  show BFalse        = "f"
  show Power2        = "pwr2"
  show I             = "i"
  show Cons          = "cons"
  show Car           = "car"
  show Cdr           = "cdr"
  show Nil           = "nil"
  show IsNil         = "isnil"
  show Draw          = "draw"
  show Checkerboard  = "checkerboard"
  show MultiDraw     = "multipledraw"
  show If0           = "if0"
  show Interact      = "interact"
  show StatelessDraw = "statelessdraw"
  show StatefulDraw  = "statefuldraw"
