-- |

{-# LANGUAGE RankNTypes #-}

module Game.Strategy where

import Types (Env)
import Game.Types
import Game.Req
import Control.Monad.IO.Class
import Control.Monad.Reader

data Strategy m = Strategy
  { initialParams :: GameInfo -> m ShipParams
  , react :: GameInfo
          -> Maybe GameState
          -> m [Command]
  , runner :: forall a. Env -> m a -> IO a
  }

strategyLoop :: (Monad m, MonadIO m, MonadReader Env m)
             => Strategy m
             -> GameResponse
             -> m ()
strategyLoop _ GameResponse{ gameStage=GameFinished } = pure ()
strategyLoop strategy gs = do
  let info = gameInfo gs
      state = gameState gs
  actions <- react strategy info state
  mgs' <- cmdReq gs actions
  strategyLoop strategy $ case mgs' of
    Left _ -> gs
    Right gs' -> gs'

strategyGame :: (Monad m, MonadFail m, MonadIO m, MonadReader Env m)
             => Strategy m
             -> m ()
strategyGame strategy = do
  world0 <- joinReq []
  ship <- initialParams strategy $ gameInfo world0
  world1 <- startReq ship
  strategyLoop strategy world1
