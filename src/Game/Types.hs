{-# LANGUAGE StrictData #-}
{-# LANGUAGE TupleSections #-}

module Game.Types where

import Types


toVList :: [V] -> V
toVList = foldr VCons VNil

fromVList :: V -> [V]
fromVList VNil = []
fromVList (VCons v vs) = v : fromVList vs

data GameStage
  = GameNotStarted
  | GameStarted
  | GameFinished
  deriving (Eq,Show,Read,Enum)

data GameRole
  = Attacker
  | Defender
  deriving (Eq,Show,Read,Enum)

type Position = (Int, Int)
type Velocity = (Int, Int)

data Ship = Ship
  { role :: GameRole
  , shipId :: Int
  , position :: Position
  , velocity :: Velocity
  , x4 :: V
  , x5 :: V
  , x6 :: V
  , x7 :: V
  } deriving (Eq, Show)

data Command
  = Accelerate Int Velocity
  | Detonate Int
  | Shoot Int Position V{- x3 -}
    -- FIXME: x3 == 1 - us, x3 == 2 - them
  | Unknown V
    -- FIXME: fork
  deriving (Eq, Show)

data GameState = GameState
  { gameTick :: Int
  , x1 :: V
  , shipsAndCommands :: [(Ship, [Command])]
    -- FIXME: encoding of commands differs, right now sid==0
  } deriving (Eq, Show)

data Planet = Planet
  { radius :: Int
  , mass   :: Int
  } deriving (Eq, Show)

data GameInfo = GameInfo
  { u0 :: V
  , myRole :: GameRole
  , u2 :: V
  , u3 :: Planet
  , u4 :: V
    -- FIXME: u4 - enemy's ship params
  } deriving (Eq, Show)

data GameResponse = GameResponse
  { status :: Integer
  , gameStage :: GameStage
  , gameInfo :: GameInfo
  , gameState :: Maybe GameState
  } deriving (Eq, Show)

type ShipParams = (Integer, Integer, Integer, Integer)


accel :: Int -> Velocity -> Command
accel = Accelerate

shoot :: Int -> Position -> Int -> Command
shoot ident pos x3 = Shoot ident pos $ N $ fromIntegral x3


mismatch :: Show a => String -> a -> Either String b
mismatch expected actual = Left $ "Expected " ++ expected ++ ", got " ++ show actual

decodePlanet :: V -> Either String Planet
decodePlanet (VCons (N r) (VCons (N m) VNil)) = Right $ Planet (fromIntegral r) (fromIntegral m)
decodePlanet v = mismatch "Planet" v

decodeGameInfo :: V -> Either String GameInfo
decodeGameInfo vl = case fromVList vl of
  [u0, N role, u2, u3, u4] -> do
    planet <- decodePlanet u3
    pure $ GameInfo u0 (toEnum $ fromIntegral role) u2 planet u4
  r -> mismatch "GameInfo - list of 5" r

decodePair :: V -> Either String (Int, Int)
decodePair (VCons (N x) (N y)) = Right (fromIntegral x, fromIntegral y)
decodePair v = mismatch "pair" v

encodePair :: (Int, Int) -> V
encodePair (x, y) = VCons (N $ fromIntegral x) (N $ fromIntegral y)

decodeShip :: V -> Either String Ship
decodeShip vl = case fromVList vl of
  [N role, N sid, pos, vel, x4, x5, x6, x7] -> do
    let r = toEnum $ fromIntegral role
        shipId = fromIntegral sid
    position <- decodePair pos
    velocity <- decodePair vel
    pure $ Ship r shipId position velocity x4 x5 x6 x7
  v -> mismatch "Ship - list of 8" v

decodeCommand :: V -> Either String Command
decodeCommand (VCons (N 0) (VCons vec VNil)) = Accelerate (fromIntegral 0) <$> decodePair vec
decodeCommand (VCons (N 1) VNil) = pure $ Detonate (fromIntegral 0)
decodeCommand (VCons (N 2) (VCons target v)) = (\t -> Shoot (fromIntegral 0) t v) <$> decodePair target
decodeCommand v = pure $ Unknown v

encodeCommand :: Command -> V
encodeCommand (Accelerate sid vec) = VCons (N 0) (VCons (N $ fromIntegral sid) (VCons (encodePair vec) VNil))
encodeCommand (Detonate sid) = VCons (N 1) (VCons (N $ fromIntegral sid) VNil)
encodeCommand (Shoot sid pos x) = VCons (N 2) (VCons (N $ fromIntegral sid) (VCons (encodePair pos) (VCons x VNil)))

decodeShipAndCommands :: V -> Either String (Ship, [Command])
decodeShipAndCommands vl = case fromVList vl of
  [ship, cmds] -> do
    s <- decodeShip ship
    (s,) <$> mapM decodeCommand (fromVList cmds)
  v -> mismatch "Ship, [Command]" v

decodeShipsAndCommands :: V -> Either String [(Ship, [Command])]
decodeShipsAndCommands = mapM decodeShipAndCommands . fromVList

decodeGameState :: V -> Either String (Maybe GameState)
decodeGameState vl = case fromVList vl of
  [N tick, x1, shipsAndCommands] -> do
    let gt = fromIntegral tick
    sncs <- decodeShipsAndCommands shipsAndCommands
    pure $ Just $ GameState gt x1 sncs
  [] -> pure Nothing
  v -> mismatch "Game state" v

toGameResponse :: V -> Either String GameResponse
toGameResponse vl = case fromVList vl of
  [N status, N stage, info, state] -> do
    gi <- decodeGameInfo info
    mgs <- decodeGameState state
    pure $ GameResponse status (toEnum $ fromIntegral stage) gi mgs
  v -> mismatch "GameResponse" v
