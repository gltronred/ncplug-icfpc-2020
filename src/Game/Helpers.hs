-- |

module Game.Helpers where

import Game.Types

import Control.Monad.IO.Class
import System.Random
import Text.Printf

type R4 = (V2, V2)

findMyShip :: GameRole -> GameState -> (Ship, [Command])
findMyShip r GameState{ shipsAndCommands = scs }
  = head $ filter ((==r) . role . fst) scs

findOtherShips :: GameRole -> GameState -> [(Ship, [Command])]
findOtherShips r GameState{ shipsAndCommands = scs }
  = filter ((/=r) . role . fst) scs

rand :: MonadIO m => (Int,Int) -> m Int
rand = liftIO . randomRIO

add :: Num a => (a,a) -> (a,a) -> (a,a)
add (x,y) (dx,dy) = (x+dx, y+dy)

diff :: Num a => (a,a) -> (a,a) -> (a,a)
diff (x,y) (px,py) = (x-px, y-py)

type V2 = (Double, Double)

toV2 :: Velocity -> V2
toV2 (x, y) = (fromIntegral x, fromIntegral y)

fromV2 :: V2 -> Velocity
fromV2 (x, y) = (ceiling x, ceiling y)

roundV2 :: V2 -> Velocity
roundV2 (x, y) = (round x, round y)

len :: V2 -> Double
len (x, y) = sqrt (x*x + y*y)

scale :: Double -> V2 -> V2
scale s (x, y) = (s*x, s*y)

norm :: V2 -> V2
norm v = scale (1 / len v) v

cap :: Double -> V2 -> V2
cap limit v = let
  k = len v / limit
  in if k > 1
     then scale (1 / k) v
     else v

capEach :: Int -> Velocity -> Velocity
capEach limit (x,y) = (c x, c y)
  where c a | abs a > limit = limit
            | otherwise = a

rot90ccw :: V2 -> V2
rot90ccw (x, y) = (-y, x)

showV2 :: V2 -> String
showV2 (x, y) = printf "(%.3f, %.3f)" x y


type PIDparams a = (a, a, a)

-- | Assuming dt = 1
piReg :: Num a
      => PIDparams a
      -> a     -- accumulated eror
      -> a     -- current value
      -> a     -- desired value
      -> ( a   -- correction to apply
         , a)  -- new accumulated error to remember
piReg (kp, ki, _) accum curr target = (kp*e + ki*i , i)
  where
    e = target - curr
    i = accum + e

-- | Assuming dt = 1
pidReg :: Num a
       => PIDparams a
       -> a     -- accumulated eror
       -> a     -- previous error
       -> a     -- current value
       -> a     -- desired value
       -> ( a   -- correction to apply
          , a   -- new accumulated error to remember
          , a)  -- new previous error to remember
pidReg (kp, ki, kd) accum prev curr target = (kp*e + ki*i + kd*d, i, e)
  where
    e = target - curr
    i = accum + e
    d = e - prev
