-- |

module Game.Req where

import Types
import Modulation (modulate, demodulate)
import Http
import Game.Types

import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B

makeReq :: (MonadReader Env m, MonadIO m) => [V] -> m (Either String ByteString)
makeReq vs = do
  let req = toVList vs
      Right (W w) = modulate req
  liftIO $ putStrLn $ "<<< " ++ show req
  send w

createReq :: (MonadReader Env m, MonadIO m) => m (Integer, Integer)
createReq = do
  let req = toVList [ N 1, N 0 ]
      Right (W w) = modulate req
  liftIO $ putStrLn $ "<<< " ++ show req
  ea <- send w
  case ea of
    Left e -> do
      liftIO $ putStrLn $ "Fail to request: " ++ e
      error e
    Right w -> case demodulate w of
      Left e -> do
        liftIO $ putStrLn $ "Fail to decode: " ++ e ++ ": " ++ B.unpack w
        error e
      Right v -> do
        liftIO $ putStrLn $ ">>> State: " ++ show v
        case fromVList v of
          [N 1, l] -> case map ((!!1) . fromVList) $ fromVList l of
            [N attacker, N defender] -> pure (attacker, defender)
            _ -> do
              liftIO $ putStrLn $ "Failed to parse response: " ++ show (fromVList l)
              error "Failed to parse"
          _ -> do
            liftIO $ putStrLn $ "Failed to parse response: " ++ show (fromVList v)
            error "Failed to parse"

joinReq :: (MonadFail m, MonadReader Env m, MonadIO m)
        => [V]
        -> m GameResponse
joinReq vs = do
  key <- asks envPlayerKey
  let list = toVList vs
  resp <- makeReq [N 2, N key, list]
  Right a <- parseResp resp
  pure a

startReq :: (MonadFail m, MonadIO m, MonadReader Env m)
         => (Integer, Integer, Integer, Integer)
         -> m GameResponse
startReq (a,b,c,d) = do
  key <- asks envPlayerKey
  resp <- makeReq [N 3, N key, toVList $ map N [a,b,c,d]]
  Right a <- parseResp resp
  pure a

cmdReq :: (MonadIO m, MonadReader Env m)
       => GameResponse
       -> [Command]
       -> m (Either String GameResponse)
cmdReq gs cmds = do
  key <- asks envPlayerKey
  liftIO $ putStrLn $ "<<< Cmds: " ++ show cmds
  resp <- makeReq [N 4, N key, toVList . map encodeCommand $ cmds]
  parseResp resp

parseResp :: MonadIO m => Either String ByteString -> m (Either String GameResponse)
parseResp (Left e) = do
  liftIO $ putStrLn $ "Fail to request: " ++ e
  error e
parseResp (Right w) = case demodulate w of
  Left e -> do
    liftIO $ putStrLn $ "Fail to decode: " ++ e ++ ": " ++ B.unpack w
    error e
  Right v -> do
    liftIO $ putStrLn $ ">>> State: " ++ show v
    let mst = toGameResponse v
    liftIO $ case mst of
      Left err -> putStrLn $ "[ ERR ] Wrong request: " ++ show err
      Right st -> liftIO $ putStrLn $ ">>> Converted: " ++ show st
    pure mst
