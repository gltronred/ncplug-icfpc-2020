-- | Simple strategy
--
-- # Movements
--
-- 1. Try to be inside of square of game field
-- 2. Try to be outside of square surrounding planet
-- 3. If we are approaching either square, apply acceleration
--
-- # Shoot
--
-- 4. Try and shoot to predicted point
-- 4.1. Target: p + v
--
-- # Notes
--
-- Probably, in game info
-- * `x2` is bounding box
-- * `x3` is gravity
-- * `x4` is planet radius

module Game.Strategy1
  ( stayAndShoot ) where

import Types (Env, V(..))
import Game.Types
import Game.Strategy
import Game.Helpers

import Control.Monad.RWS
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.List

data St = St
  { bulletsLeft :: Int
  , shotTime :: Int
  , errAcc :: Double -- for PI control
  , traj :: IntMap [R4]
  , myPath :: [Position]
  } deriving (Eq,Show,Read)

type SAS = RWST Env () St IO

-- Kinda arbitrary constants to bruteforce
piParams :: PIDparams Double
piParams = (3.0, 1.5, 0) -- last coefficient is irrelevant for a PI-control

initialState :: St
initialState = St 0 0 0 IM.empty []

stayAndShoot :: Strategy SAS
stayAndShoot = Strategy
  { initialParams = params
  , react = work
  , runner = \env act -> fst <$> evalRWST act env initialState
  }

params :: GameInfo -> SAS ShipParams
params info = pure (256,13,11,1)

work :: GameInfo -> Maybe GameState -> SAS [Command]
work info Nothing = do
  let me = myRole info
  pure []
work info (Just state) = do
  err <- gets errAcc
  let tick = gameTick state
      me = myRole info
      planetR = radius $ u3 info
      myShip = fst $ findMyShip me state
      others = findOtherShips me state
      ident = shipId myShip
      -- moving
      v = velocity myShip
      pos@(x, y) = position myShip
      (acc, newErr) = calcAccel planetR err myShip
  -- shooting
  allOlds <- gets traj
  let toR4 other = (toV2 $ position other, toV2 $ velocity other)
      allCur = IM.fromList $ map (\(s,_) -> (shipId s, toR4 s)) others
      allPredictionNews = multiPredict allOlds allCur
      -- select nearest target
      distMe :: Position -> Double
      distMe r = len $ toV2 r `diff` toV2 (x,y)
      -- no ship goes that far!
      nullShip = (ident, (10000,10000))
      -- target nearest to us
      minDist ident a cur = if distMe a < distMe (snd cur)
                            then (ident, a)
                            else cur
      selectPrediction :: IntMap Position -> (Int, Position)
      selectPrediction = IM.foldrWithKey minDist nullShip
      (tgt, predicted) = selectPrediction $ IM.map fst allPredictionNews
      -- for updating trajectories
      allNews = IM.map snd allPredictionNews
  modify $ \s -> s { traj = allNews }
  -- save path
  modify $ \s -> s { myPath = take 4 $ (x,y) : myPath s }
  myWay <- gets myPath
  let me3 = last myWay
      me0 = head myWay
  -- zones
  let isNearPlanet = abs x < 40 && abs y < 40
      isOuterSpace = abs x > 120 || abs y > 120
      isSittingDuck = (<1) $ len $ toV2 $ me3 `diff` me0 -- len (toV2 v) < 1
      isSlow = len (toV2 v) < 7
      -- away
      size = 16
      maxA = 2
      maxA1= 1
      away = case (abs x, abs y) of
        (ux, uy) | ux < size -> capEach maxA (0, y)
                 | uy < size -> capEach maxA (x, 0)
                 | otherwise -> capEach maxA1(x, y)
  randV <- (,) <$> rand (0,1) <*> rand (0,1)
  -- logging
  liftIO $ putStrLn $ concat [ "[ ZONES ]"
                             , " rad=", show (len $ toV2 pos)
                             , " near=", show isNearPlanet
                             , " outer=", show isOuterSpace
                             , " duck=", show isSittingDuck
                             , " slow=", show isSlow
                             , " random dV=", show randV
                             ]
  liftIO $ putStrLn $ concat [ "[ PATH ]"
                             , " path=", show myWay
                             ]
  liftIO $ putStrLn $ concat [ "[ LOG ]"
                             , " acc=", show acc
                             ]
  liftIO $ putStrLn $ concat [ "[ LOG ]"
                             , " olds=", show allOlds
                             , " news=", show allNews
                             , " | target=", show tgt
                             , " pred=", show predicted
                             ]
  liftIO $ putStrLn $ concat [ "[ APPLY ]"
                             , " away=", show away
                             , " | target=", show tgt
                             , " pred=", show predicted
                             , " | rand=", show randV
                             ]
  pure $ pure $ case tick of
    _
      -- | isNearPlanet -> accel ident away
      | isOuterSpace -> shoot ident predicted 1
      | len (toV2 pos) > 3 * fromIntegral planetR &&
        len (toV2 pos) < 6 * fromIntegral planetR -> shoot ident predicted 1
      | isSittingDuck || acc == (0,0)
        -> accel ident randV
      | otherwise -> accel ident acc

calcAccel :: Int -> Double -> Ship -> (Velocity, Double)
calcAccel planetR accErr myShip = (acc, newErr)
  where
    -- Kinda arbitrary constant
    targetR = fromIntegral $ 5*planetR
    v = velocity myShip
    pos@(x, y) = position myShip
    p2 = toV2 pos
    currR = len p2
    (dR, newErr) = piReg piParams accErr currR targetR
    s = dR / currR
    outwards = scale (-s) p2
    around = norm $ rot90ccw p2
    accCap = cap 1.5 $ outwards `add` around
    acc = fromV2 accCap

multiPredict :: IntMap [R4] -> IntMap R4 -> IntMap (Position, [R4])
multiPredict = IM.mergeWithKey combine (const IM.empty) (IM.map initialise)
  where initialise :: R4 -> (Position, [R4])
        initialise r@(p,_) = (roundV2 p, pure r)
        combine :: Int -> [R4] -> R4 -> Maybe (Position, [R4])
        combine _ident olds cur = Just $ predict $ cur : olds

predict :: [R4] -> (Position, [R4])
predict new = let
  -- alpha = 2 / (fromIntegral maxLen + 1)
  -- vema = foldr1 (\a b -> scale alpha a `add` scale (1-alpha) b) $ map snd new
  cur = head new
  n = length new
  vs = map snd new
  suma = foldr add (0,0) $ zipWith diff vs $ tail vs
  a = scale (1 / fromIntegral n) suma
  v = snd cur
  p = fst cur
  prediction = p `add` v `add` scale 0.5 suma
  in (roundV2 prediction, new)
