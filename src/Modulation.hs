-- |

{-# LANGUAGE OverloadedStrings #-}

module Modulation where

import Types

import Control.Monad
import Data.Bits
import Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B
import Data.List (foldl')
import Data.Serialize

modulate (N n) = Right $ W $ runPutLazy $ putter $ N n
modulate v@VNil = Right $ W $ runPutLazy $ putter v
modulate v@(VCons _ _) = Right $ W $ runPutLazy $ putter v
modulate other = Left $ "Don't know how to modulate " <> show other

-- modulate :: V -> Either String V
-- modulate (N n)
--   | n >=0 = Right $ W $ "01" <> modInt n
--   | n < 0 = Right $ W $ "10" <> modInt (-n)
-- modulate VNil = Right $ W $ "00"
-- modulate (VCons v1 v2) = do
--   W w1 <- modulate v1
--   W w2 <- modulate v2
--   pure $ W $ "11" <> w1 <> w2
-- modulate other = Left $ "Don't know how to modulate " <> show other

putter :: Putter V
putter (N n) = intPutter n
putter v = listPutter v

intPutter :: Putter Integer
intPutter n = do
  putSign n
  putWidth $ abs n
  putBits $ abs n

listPutter VNil = putByteString "00"
listPutter (VCons v1 v2) = do
  putByteString "11"
  putter v1
  putter v2

putSign n
  | n >=0 = putByteString "01"
  | n < 0 = putByteString "10"

putWidth n = do
  replicateM (width n) $ putByteString "1"
  putByteString "0"

putBits n = forM_ (reverse [0 .. 4 * width n - 1]) $ \i -> do
  if n `testBit` fromIntegral i
    then putByteString "1"
    else putByteString "0"

-- | `width x = w` means that x can be represented using `4w` bits
width x = go 0 1
  where go k n
          | x < n = k
          | otherwise = go (k+1) (n*16)

demodulate :: ByteString -> Either String V
demodulate = runGet parserV . B.toStrict

parserV :: Get V
parserV = do
  tag <- getBytes 2
  case tag of
    "01" -> N <$> parserInt
    "10" -> N . negate <$> parserInt
    "00" -> pure VNil
    "11" -> VCons <$> parserV <*> parserV

parserInt :: Get Integer
parserInt = do
  w <- getWidth 0
  bs <- getBytes $ 4*w
  pure $ B.foldl' (\n c -> 2*n + if c == '0' then 0 else 1) 0 $ B.fromStrict bs

getWidth :: Int -> Get Int
getWidth k = do
  b <- getBytes 1
  case b of
    "0" -> pure k
    "1" -> getWidth $ k+1
