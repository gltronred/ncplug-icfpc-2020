-- |

module Explore where

import Types
import Language as Lang
import Compiler as Compiler
import Interpreter (eval, evalN)
import Simple (simpleEvalEnv)

import Control.Monad.IO.Class
import Control.Monad.Reader
import System.IO

gSize :: Int -> (Int -> Int -> Int) -> Tree -> Int
gSize z op (App t1 t2) = gSize z op t1 `op` gSize z op t2
gSize z op _ = z

size = (++ " ") . show . gSize 1 (\l r -> l + r + 1)
depth = (++ " ") . show . gSize 1 (\l r -> (l `max` r) + 1)
dot = const "."
silence = const ""

printSize :: (Tree -> String) -> Tree -> IO ()
printSize f t = do
  putStrLn $ f t
  -- hFlush stdout

prep :: String -> IO EvalEnv
prep f = do
  Right prog <- Compiler.compile Nothing . Lang.parse <$> readFile f
  let bs = Compiler.binds prog
  pure $ (simpleEvalEnv bs) { logAction = printSize depth }

apGalaxy :: V -> Integer -> Integer -> Tree
apGalaxy s x y = App (App (Fun "galaxy") (Val s)) $ Val $ VCons (N x) $ VCons (N y) VNil

getResult :: V -> Integer -> Integer -> IO ()
getResult s x y = do
  e <- prep "galaxy.txt"
  let expr = apGalaxy s x y
  r <- runReaderT (evalN expr) e
  print r
