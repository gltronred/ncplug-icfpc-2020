-- |

module Simple where

import Types

simpleEvalEnv :: Binds -> EvalEnv
simpleEvalEnv bs = EvalEnv
  { binds = bs
  , onFail = \s -> do
      putStrLn $ "======== FAIL: " ++ s
      fail s
  , logAction = print
  , debugging = putStrLn
  -- , draw = \cb v -> do
  --     putStrLn $ "==== DRAW: " ++ show cb
  --     print v
  -- , send = \v -> do
  --     putStrLn "==== SEND: "
  --     print v
  --     read <$> getLine
  }
